// Firebase App (the core Firebase SDK) is always required and must be listed first
import * as firebase from "firebase/app";

// If you enabled Analytics in your project, add the Firebase SDK for Analytics
import "firebase/analytics";

// Add the Firebase products that you want to use
import "firebase/auth";
import "firebase/firestore";

const firebaseConfig = {
  apiKey: "AIzaSyAaMAiY6ac03XPlrv9bTjmpsAXIN8F9r7E",
  authDomain: "onuwerewolf.firebaseapp.com",
  databaseURL: "https://onuwerewolf.firebaseio.com",
  projectId: "onuwerewolf",
  storageBucket: "onuwerewolf.appspot.com",
  messagingSenderId: "161563204023",
  appId: "1:161563204023:web:29ba42cb22b09ddcb2b032",
  measurementId: "G-1FZZJ1TFR6",
};
// Initialize Firebase
firebase.initializeApp(firebaseConfig);

// firebase utils
const db = firebase.firestore();
const auth = firebase.auth();

export { firebase, db, auth };
