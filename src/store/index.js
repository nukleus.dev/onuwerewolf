import Vue from "vue";
import { vuexfireMutations, firestoreAction } from "vuexfire";
import Vuex from "vuex";
import * as fb from "@/firebase";

Vue.use(Vuex);

export default new Vuex.Store({
  state: {
    game: {},
  },
  mutations: {
    ...vuexfireMutations,
  },
  actions: {
    bindGame: firestoreAction(({ bindFirestoreRef }, payload) => {
      // return the promise returned by `bindFirestoreRef`
      return bindFirestoreRef(
        "game",
        fb.db.collection("games").doc(payload.gameId)
      );
    }),
    unbindGame: firestoreAction(({ unbindFirestoreRef }) => {
      unbindFirestoreRef("game");
    }),
  },
  modules: {},
});
